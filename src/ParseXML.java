import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.dom4j.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParseXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		try {

			File fXmlFile = new File(
					"C:\\Users\\VyankateshR\\Downloads\\hdn bericht DA met aan te leveren stukken.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("DocumentKenmerk");
			///NodeList nList = doc.getChildNodes();
			System.out.println("----------------------------");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				org.w3c.dom.Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println("id : " + eElement.getAttribute("Volgnummer"));
					System.out.println(
							"SoortDocument : " + eElement.getElementsByTagName("SoortDocument").item(0).getTextContent());
				}
			}
			// code to get header data
			NodeList nListHeader = doc.getElementsByTagName("Header");
			///NodeList nList = doc.getChildNodes();
			System.out.println("----------------------------");
			for (int temp = 0; temp < nListHeader.getLength(); temp++) {
				org.w3c.dom.Node nNode = nListHeader.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					///System.out.println("id : " + eElement.getAttribute("Volgnummer"));
					System.out.println(
							"VerzenderNrHDN : " + eElement.getElementsByTagName("VerzenderNrHDN").item(0).getTextContent());
				}
			}
			//code to get DocumentData
			NodeList nListDocument = doc.getElementsByTagName("DocumentData");
			///NodeList nList = doc.getChildNodes();
			System.out.println("----------------------------");
			for (int temp = 0; temp < nListDocument.getLength(); temp++) {
				org.w3c.dom.Node nNode = nListDocument.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					///System.out.println("id : " + eElement.getAttribute("Volgnummer"));
					System.out.println(
							"Kenmerk : " + eElement.getElementsByTagName("Kenmerk").item(0).getTextContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
